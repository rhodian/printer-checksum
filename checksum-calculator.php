<?php
spl_autoload_register(function ($class) {
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $class).'.php';
    if (file_exists($file)) {
        require_once $file;
        return true;
    }
    return false;
});

use GuzzleHttp\Client as GuzzleClient;
use Predis\Client as RedisClient;
use Services\ChecksumCalculator;

require_once './vendor/autoload.php';

Predis\Autoloader::register();

$redisClient = new RedisClient('redis://redis:6379');
//$redisClient = new RedisClient('tcp://localhost:6379');
$guzzleClient = new GuzzleClient();
$checksumCalculator = new ChecksumCalculator($guzzleClient, $redisClient);
$isValid = $checksumCalculator->isChecksumValid();
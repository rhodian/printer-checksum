<?php
namespace Services;

use GuzzleHttp\Client as GuzzleClient;
use Predis\Client as RedisClient;

class ChecksumCalculator
{
    private const CONF_VERIFY_URL = 'http://answer:3000';
    private const CONF_DIVIDES_INTO = 177;

    private $guzzleClient;
    private $redisClient;

    /**
     *
     * @param GuzzleClient $guzzleClient    Guzzle client
     * @param RedisClient $redisClient      Properly initialized Redis Client
     */
    public function __construct(GuzzleClient $guzzleClient, RedisClient $redisClient){
        $this->guzzleClient = $guzzleClient;
        $this->redisClient = $redisClient;
    }

    /**
     * Whether the checksum generated is valid
     *
     * @return bool
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function isChecksumValid():bool{
        $checksum = $this->getChecksum();

        echo " \n\n Checksum: $checksum \n\n";

        $response = $this->guzzleClient->request('GET', self::CONF_VERIFY_URL . "/$checksum");

        // Just for testing
        echo "\n".$response->getBody()."\n";

        return $response->getStatusCode() === 200;
    }

    /**
     * Calls Redis, retrieves all keys, and calculates checksum value
     *
     * @return int
     */
    protected function getChecksum():int{
        $checksum = 0;

        $keys = $this->redisClient->keys('*');
        foreach ($keys as $key) {
            $type = $this->redisClient->type($key);
            switch ($type){
                case 'set':
                    $values = $this->redisClient->smembers($key);
                    break;
                case 'list':
                    $values = $this->redisClient->lrange($key, 0, -1);
                    break;
                default:
                    throw new \LogicException("\nUnhandled key type: $type\n");
                    break;
            }

            if($this->shouldSkip($values)){
                continue;
            }

            $checksum += $this->calculateDiff($values);
        }

        return $checksum;
    }

    /**
     * Whether the Redis value should be skipped over (ie, not added to checksum value)
     *
     * @param array $values
     * @return bool
     */
    private function shouldSkip(array $values):bool{
        $shouldSkip = false;

        $arraySize = \count($values);
        for ($i = 0; $i < $arraySize - 1; $i++) {
            for ($j = $i + 1; $j < $arraySize; $j++) {
                $valA = $values[$i];
                $valB = $values[$j];
                $isAnagram = $this->isAnagram($valA, $valB);
                $isDivisible = $this->isDivisible((int) $valA, (int) $valB);
                if($isAnagram || $isDivisible){
                    $shouldSkip = true;
                    break 2;
                }
            }
        }

        return $shouldSkip;
    }

    private function calculateDiff(array $values):int{
        $max = max($values);
        $min = min($values);

        return $max - $min;
    }

    private function isAnagram($valueA, $valueB):bool{
        return count_chars($valueA, 1) == count_chars($valueB, 1);
    }

    private function isDivisible($valueA, $valueB):bool{
        return ($valueA / $valueB === self::CONF_DIVIDES_INTO) || ($valueB / $valueA  === self::CONF_DIVIDES_INTO);
    }
}